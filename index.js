(function () {
	createLengthSelector('username', () => { onLengthSelected('username'); });
	createLengthSelector('password', () => { onLengthSelected('password', true); });
})()

function createLengthSelector(id, onChanged) {
	elem = document.getElementById(id);

	let label = document.createElement('label');
	label.setAttribute('id', `${id}-length`);
	label.setAttribute('for', `${id}-length`);

	let select = document.createElement('select');
	select.setAttribute('id', `${id}-length`);
	select.onchange = onChanged;

	label.appendChild(select);

	const range = shuffle([...Array(256).keys()]
		.map(x => x + 1)); // Shift the values to range 1..n instead of 0..n
	range.forEach((i) => {
		let opt = document.createElement('option');
		opt.text = i;

		select.appendChild(opt);
	});

	elem.appendChild(label);
}

function onLengthSelected(id, maskChars) {
	let elem = document.getElementById(id);
	let select = elem.querySelector(`#${id} select`);

	if (select === null) {
		console.error("select was null");
		return;
	}

	let len = parseInt(select.value, 10);

	if (isNaN(len)) {
		console.error("Invalid selection");
		return;
	}

	elem.innerHTML = '';
	populateInput(elem, len, maskChars);
}

function populateInput(elem, len, maskChars) {
	for (i = 0; i < len; i++) {
		let label = document.createElement('label');
		label.setAttribute('id', elem.id);
		label.setAttribute('for', elem.id);
		label.style.visibility = 'hidden';

		let select = document.createElement('select');
		select.setAttribute('id', elem.id);
		select.onchange = () => { makeRandomSelectorVisible(elem); };

		label.appendChild(select);

		// Printable character code ranges
		const firstCharCode = 32;
		const lastCharCode = 127;

		const chars = shuffle([...Array(lastCharCode - firstCharCode + 1).keys()]
			.map(x => x + firstCharCode))

		chars.forEach((charCode) => {
			const char = String.fromCharCode(charCode);

			let opt = document.createElement('option');
			opt.setAttribute('value', char);

			if (maskChars) {
				opt.text = '*';
			} else {
				opt.text = char;
			}

			select.appendChild(opt);
		});

		elem.appendChild(label);
	}

	makeRandomSelectorVisible(elem)
}

function makeRandomSelectorVisible(parent) {
	let labels = parent.querySelectorAll('label');
	let hiddenElementIndices = [];

	for (i = 0; i < labels.length; i++) {
		if (labels[i].style.visibility === 'hidden') {
			hiddenElementIndices.push(i);
		}
	}

	if (hiddenElementIndices.length > 0) {
		const idx = hiddenElementIndices[Math.floor(Math.random() * hiddenElementIndices.length)];
		labels[parseInt(idx, 10)].style.visibility = 'inherit';
	}
}

// Source: https://stackoverflow.com/a/15585372
function shuffle(o) {
	for (var j, x, i = o.length; i; j = parseInt(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
	return o;
}

function onSubmit() {
	const username = getSelection('username');
	const password = getSelection('password');

	alert(`Username: "${username}" - Password: "${password}"`);
}

function getSelection(id) {
	elem = document.getElementById(id);
	chars = elem.querySelectorAll('select');

	let str = '';
	chars.forEach((e) => { str += e.value; });

	return str;
}
